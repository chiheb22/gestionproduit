package com.ck.service;

import com.ck.DAO.AppRoleRepository;
import com.ck.DAO.AppUserRepository;
import com.ck.entities.AppRole;
import com.ck.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional//les methodes sont transactionnel s'execute en totalite
// ou rollback vers la dernier etape enregistrer(pas necessaire d'utiliser save car les
// methodes faire des commites à la fin de chaque methode)
public class AccountServiceImpl implements AccountService {
    public AccountServiceImpl(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    private AppUserRepository appUserRepository;
    private AppRoleRepository appRoleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public AppUser saveUser(String username, String password, String confirmedPassword) {
        AppUser user = appUserRepository.findByUsername(username);

        if (user != null)
            throw new  RuntimeException("User already exits");
        if(!password.equals(confirmedPassword))
            throw new  RuntimeException("Wrong Confirmation password");
        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(bCryptPasswordEncoder.encode(password));
        appUser.setActivated(true);
        appUserRepository.save(appUser);
        addRoleToUser(username,"USER");
        return appUser;
    }

    @Override
    public AppRole save(AppRole role) {

        return appRoleRepository.save(role);
    }

    @Override
    public AppUser loadUserByUsername(String username)
    {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
    AppUser appUser = appUserRepository.findByUsername(username);
    AppRole appRole = appRoleRepository.findByRoleName(roleName);
    appUser.getRoles().add(appRole);
    appUserRepository.save(appUser);
    }
}
