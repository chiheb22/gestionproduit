package com.ck.service;

import com.ck.entities.AppRole;
import com.ck.entities.AppUser;

public interface AccountService {
public AppUser saveUser(String username , String password , String confirmedPassword);
public AppRole save(AppRole role);
public AppUser loadUserByUsername(String username);
public void addRoleToUser(String username , String roleName);
}
