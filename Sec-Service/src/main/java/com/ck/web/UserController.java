package com.ck.web;

import com.ck.entities.AppUser;
import com.ck.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/registre")
    public AppUser regitre(@RequestBody UserForm userForm)
    {
        return accountService.saveUser(userForm.getUsername(),userForm.getPassword(), userForm.getConfirmedPassword());
    }

}
@Data
class UserForm{
    private String username;
    private String password;
    private String confirmedPassword;
}