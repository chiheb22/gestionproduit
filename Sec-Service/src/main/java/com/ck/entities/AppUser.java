package com.ck.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)// lorsque l'objet json s'expose il masque le mot de passe(securite)
    private String password;

    private  boolean activated;

    @ManyToMany(fetch = FetchType.EAGER)//charger les roles de chaque user et pas seulement le ID
    private Collection<AppRole> roles = new ArrayList<>();
}
