package com.ck.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
@Document// collection de type JSON
@Data // getters and setters
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    @Id
    private String ID;// mongodb => id de type string
    private String Name;
    @DBRef // enregister que le id du product mais pas la totalite d'objet
    private Collection<Product> products = new ArrayList<>();

    @Override
    public String toString() {
        return "Category{" +
                "ID='" + ID + '\'' +
                ", Name='" + Name + '\'' +
                '}';
    }
}
