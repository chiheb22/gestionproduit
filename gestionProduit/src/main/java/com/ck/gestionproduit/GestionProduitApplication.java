package com.ck.gestionproduit;

import com.ck.Repository.CategoryRepository;
import com.ck.Repository.ProductRepository;
import com.ck.entities.Category;
import com.ck.entities.Product;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.stream.Stream;

@SpringBootApplication(scanBasePackages = "com.ck")
@EnableMongoRepositories("com.ck.Repository")// to detect mongo repo
@EnableEurekaClient
@OpenAPIDefinition
public class GestionProduitApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionProduitApplication.class, args);
    }
    @Bean
CommandLineRunner start(ProductRepository productRepository, CategoryRepository categoryRepository){
        return args -> {
            categoryRepository.deleteAll();

            Stream.of("INFOGRAPHICS 1","MAPS 2").forEach(c->{
                categoryRepository.save(new Category(c.split(" ")[1],c,new ArrayList<>()));
            });
            categoryRepository.findAll().forEach(System.out::println);

            productRepository.deleteAll();

            String DescriptionC1 = "Since its inception in 1989, the internet has been in a state of continuous flux as millions of different sites emerged and then fell by the wayside." +
                    " From this initial chaos, many major corporate players have emerged over the decades, and have since become dominant in heir respective niches. This trend has been especially evident in the last decade, as Google, Youtube, Amazon and Facebook have far outgrown or absorbed most of their previous competition on an international global scale." +
                    " Increasingly, this has led to mounting accusations of monopolistic behavior, censorship and misuse of user data.";
            Category c1 =categoryRepository.findById("1").get();
            Stream.of("MAP OF THE INTERNET 2021").forEach(p->
            {Product p1 =productRepository.save((new Product(null,p,14.99,DescriptionC1,c1)));
                c1.getProducts().add(p1);
                categoryRepository.save(c1);
            });


            String DescriptionC2 = "Over most of its history, Earth has been going through a so-called “supercontinent cycle“. As continents shift, major landmasses tend to eventually drift together and coalesce into a single supercontinent, surrounded by a single super-ocean. The most well-known such landmass is “Pangea“ which existed during most of the era of the dinosaurs, which was preceded by Pannotia, Rodinia and several others. However, as trapped heat from the  Earth’s mantle slowly builds up in the center of this global landmass, faults and rift valleys appear (such as the Great Rift Valley in modern Africa). Cratons drift away from each other as the supercontinent breaks up, setting the stage for the next cycle once again\n" +
                    "\n";

            Category c2 =categoryRepository.findById("2").get();
            Stream.of("CONTINENTAL DRIFT").forEach(p->
            {Product p2=productRepository.save((new Product(null,p,10.99,DescriptionC2,c2)));
                c2.getProducts().add(p2);
                categoryRepository.save(c2);
            });
            productRepository.findAll().forEach(a->{System.out.println(a.toString());
            });
        };

}
}
