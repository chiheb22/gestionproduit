package com.ck.Repository;

import com.ck.entities.Category;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource

//expose cet interface pour l'utiliser dans le Restcontroller
public interface CategoryRepository extends MongoRepository<Category,String> {
}
