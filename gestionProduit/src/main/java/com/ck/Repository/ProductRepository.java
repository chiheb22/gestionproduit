package com.ck.Repository;

import com.ck.entities.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@RepositoryRestResource

public interface ProductRepository  extends MongoRepository<Product,String>//extends mongo repo pour avoir des methode comme saveall
{
}
